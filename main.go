package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func main() {
	p := &Person{
		Name:  "John Doe",
		Id:    42,
		Email: "j.doe@example.org",
		Phones: []*Person_PhoneNumber{
			{
				Number: "+420 911",
				Type:   Person_WORK,
			},
		},
		LastUpdated: timestamppb.New(
			time.Now(),
		),
	}

	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	enc.SetIndent("", "\t")
	enc.Encode(p)
	fmt.Println(buf.String())

	protoOpts := protojson.MarshalOptions{
		Multiline: true,
		Indent:    "\t",
	}
	b, _ := protoOpts.Marshal(p)
	fmt.Println(string(b))
}
